<?php

$templatepath = get_template_directory();

define('T_THEME', get_template_directory_uri());

add_filter( 'use_block_editor_for_post', '__return_false' );

//////////////////////////////////////////////////////////////////////////////////
// PHPMailer
//////////////////////////////////////////////////////////////////////////////////

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require get_template_directory().'/phpmailer/src/Exception.php';
require get_template_directory().'/phpmailer/src/PHPMailer.php';
require get_template_directory().'/phpmailer/src/SMTP.php';

//////////////////////////////////////////////////////////////////////////////////
// Add automatic image sizes
//////////////////////////////////////////////////////////////////////////////////
if ( function_exists( 'add_image_size' ) ) { 
	//add_image_size( 'size-img01', 115, 115, array( 'center', 'center' ));
	//add_image_size( 'size-img02', 320, 210, array( 'center', 'center' ));
}

//////////////////////////////////////////////////////////////////////////////////
// Custom themes
//////////////////////////////////////////////////////////////////////////////////
function custom_theme_setup() {
	add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'custom_theme_setup');

function hide_admin_bar_from_front_end(){
	if (is_blog_admin()) {
		return true;
	}
	return false;
}
add_filter('show_admin_bar', 'hide_admin_bar_from_front_end');

/* Google Map API
------------------------------------------------------------------------------- */
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyBVE7BWWYfdZZ17452BwkaUPeBowp1qAUg');
}
add_action('acf/init', 'my_acf_init');

/* Lands
------------------------------------------------------------------------------- */
function create_land() {
	register_post_type( 'land',
		array(
			'labels' 			  	=> array(
				'name' 			  	=> __( 'Lands' ),
				'singular_name'   	=> __( 'Land' ),
				'menu_name'       	=> __('Land'),
			),
			'public'              	=> true,
			'publicly_queryable'  	=> true,
			'has_archive'         	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'query_var'           	=> true,
			'map_meta_cap'        	=> true,
			'hierarchical'        	=> false,
			'menu_position'       	=> 4,
			'rewrite'             	=> array( 'slug' => 'land' ),
			'supports' 			  	=> array('title','editor','thumbnail'),
			'show_in_rest'        	=> true,
			'rest_base'           	=> 'land',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);
	register_taxonomy( 'land_cat', 'land', array(
		'labels' 				=> array(
			'name' 				=> _x( 'Categories', 'land' ),
			'singular_name' 	=> _x( 'Category', 'land' ),
			'all_items' 		=> __( 'All Categories' ),
			'parent_item' 		=> null,
			'parent_item_colon' => null,
			'edit_item' 		=> __( 'Edit Category' ), 
			'update_item' 		=> __( 'Update Category' ),
			'add_new_item' 		=> __( 'Add New Category' ),
			'new_item_name' 	=> __( 'New Category Name' ),
			'menu_name' 		=> __( 'Categories' ),
		),
		'hierarchical' 			=> true,
		'show_admin_column' 	=> true,
		'query_var'         	=> true,
		'rewrite'           	=> array( 'slug' => 'land_cat' ),
		'show_ui'           	=> true,
		'show_in_rest'      	=> true,
		'rest_base'         	=> 'land_cat',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	));
}
add_action( 'init', 'create_land' );

/* Contact
------------------------------------------------------------------------------- */
function create_contact() {
	register_post_type( 'contact',
		array(
			'labels' 			  	=> array(
				'name' 			  	=> __( 'Contact' ),
				'singular_name'   	=> __( 'Contact' ),
				'menu_name'       	=> __('Contact'),
			),
			'public'              	=> true,
			'publicly_queryable'  	=> true,
			'has_archive'         	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'query_var'           	=> true,
			'map_meta_cap'        	=> true,
			'hierarchical'        	=> false,
			'menu_position'       	=> 4,
			'rewrite'             	=> array( 'slug' => 'contact' ),
			'supports' 			  	=> array('title','thumbnail'),
			'show_in_rest'        	=> true,
			'rest_base'           	=> 'contact',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);
}
add_action( 'init', 'create_contact' );


//////////////////////////////////////////////////////////////////////////////////
// Rest API
//////////////////////////////////////////////////////////////////////////////////

/* Sent mail
------------------------------------------------------------------------------- */
// add_action( 'rest_api_init', function () {
//   register_rest_route( 'contact/v2', '/add', array(
//     'methods' => 'POST',
//     'callback' => 'contact_func',
//   ));
// });

// function contact_func($data) {
// 	$post_id = wp_insert_post(array(
// 		//'ID' => $data['ID'],
// 		'post_title'		=> $data['name'],
// 		'post_type'			=> 'contact',		
// 	));

// 	//update_field(the_field('email'), $data['name'], $post_id );

// 	if($post_id > 0 && !is_wp_error($post_id)) {
// 		header("Access-Control-Allow-Origin: *");
// 		$mail = new PHPMailer(true);

// 		try {
// 			//Server settings
// 			$mail->SMTPDebug = SMTP::DEBUG_SERVER;
// 			$mail->isSMTP();
// 			$mail->Host       = 'smtp.gmail.com';
// 			$mail->SMTPAuth   = true;
// 			$mail->Username   = 'syvv@icd-vn.com';
// 			$mail->Password   = 'wgyzbietvxlbyyzm';
// 			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
// 			$mail->Port       = 587;

// 			//Recipients
// 			$mail->setFrom('syvv@icd-vn.com', $data['Name']);
// 			$mail->addAddress('syvv@icd-vn.com');
// 			$mail->addAddress($data['email']);

// 			$body= '';
// 			if(isset($data['name'])) {
// 				$body .= 'Name:'.$data['name'];
// 			}
// 			if(isset($data['email'])) {
// 				$body .= 'Email:'.$data['email'];
// 			}
// 			if(isset($data['tel'])) {
// 				$body .= 'Email:'.$data['email'];
// 			}
// 			//Content
// 			$mail->isHTML(true);
// 			$mail->Subject = $data['name'].'-'.$data['email'];
// 			$mail->Body    =  $body;

// 			$mail->send();
// 			return 'Message has been sent';
// 		} catch (Exception $e) {
// 			return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
// 		}	
// 	}
// }

add_action( 'rest_api_init', function () {
	register_rest_route( 'contact/v2', '/sent', array(
		'methods' => 'POST',
		'callback' => 'sentmail_func',
	));
});

function sentmail_func($data) {
	header("Access-Control-Allow-Origin: *");

	//Sent mail to Admin
	$Mail = new PHPMailer(true);	
	try {
		//Server settings
		$Mail->SMTPDebug = SMTP::DEBUG_SERVER;
		$Mail->isSMTP();
		$Mail->Host       = 'smtp.gmail.com';
		$Mail->SMTPAuth   = true;
		$Mail->Username   = 'syvv@icd-vn.com';
		$Mail->Password   = 'wgyzbietvxlbyyzm';
		$Mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
		$Mail->Port       = 587;

		//Recipients
		$Mail->setFrom('syvv@icd-vn.com', 'Houzez');
		$Mail->addAddress('syvv@icd-vn.com');
		//$Mail->addAddress($data['email']);

		$bodyAdmin= '';
		if(isset($data['name'])) {
			$bodyAdmin .= 'Nameaaa: '.$data['name'].'<br>';
		}
		if(isset($data['email'])) {
			$bodyAdmin .= 'Email: '.$data['email'].'<br>';
		}
		if(isset($data['tel'])) {
			$bodyAdmin .= 'Tel: '.$data['tel'].'<br>';
		}
		if(isset($data['message'])) {
			$bodyAdmin .= 'Message: '.$data['message'];
		}

		$Mail->isHTML(true);			
		$Mail->Subject = 'Houzez - '.$data['name'];
		$Mail->Body    =  $bodyAdmin;
		$Mail->send();

		return 'Message has been sent';
	} catch (Exception $e) {
		return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}
}

/* Add field
------------------------------------------------------------------------------- */

// Add various fields to the JSON output
function bds_register_fields() {

	//register fields post type land
	register_rest_field( 'land','image_src',
		array(
			'get_callback' => 'bds_get_image_src',
			'update_callback' => null,
			'schema' => null
		)
	);
	register_rest_field( 'land', 'land_date',
		array(
			'get_callback'    => function() {
				return get_the_date('d/m/Y');
			},
			'update_callback' => null,
			'schema'          => null,
		)
	);
	register_rest_field( 'land', 'categories',
		array(
			'get_callback'    => 'get_category_land',
			'update_callback' => null,
			'schema'          => null,
		)
	);

    //register fields post type post
	register_rest_field( 'post','image_src',
		array(
			'get_callback' => 'bds_get_image_src',
			'update_callback' => null,
			'schema' => null
		)
	);
	register_rest_field( 'post', 'news_date',
		array(
			'get_callback'    => function() {
				return get_the_date('d/m/Y');
			},
			'update_callback' => null,
			'schema'          => null,
		)
	);
}
add_action( 'rest_api_init', 'bds_register_fields' );

function bds_get_image_src( $object, $field_name, $request ) {
	if($object['featured_media'] == 0) {
		return $object['featured_media'];
	}
	$feat_img_array = wp_get_attachment_image_src($object['featured_media'], 'full', true);
	return $feat_img_array[0];
}

function get_category_land($object){
	$post_categories = array();
	$categories = wp_get_post_terms( $object['id'], 'land_cat', array('fields'=>'all') );
	foreach ($categories as $term) {
		$term_link = get_term_link($term);
		if ( is_wp_error( $term_link ) ) {
			continue;
		}
		$post_categories[] = array('term_id'=>$term->term_id, 'name'=>$term->name, 'link'=>$term_link);
	}
	return $post_categories;
}