import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Swiper from 'swiper';
import SwiperCore, { Pagination, Navigation, Mousewheel } from 'swiper/core';
import Land from '../components/Land';
import New from '../components/New';
import ApiDefault from '../api/ApiDefault';


SwiperCore.use([Pagination, Navigation, Mousewheel]);
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lands: [],
            news: [],
        }
    }

    componentDidMount() {
        const rootId = document.getElementById("root");
        var htitle = document.getElementById("htitle");
        htitle.style.animation = 'none';
        var mySwiper = new Swiper(".c-swiper", {
            init: false,
            direction: "vertical",
            slidesPerView: 1,
            spaceBetween: 0,
            mousewheel: true,
            speed: 1000,
            loop: true,
            pagination: {
                el: '.c-swiper__pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.c-swiper__next',
                prevEl: '.c-swiper__prev',
            },
            on: {
                init: function () {
                    rootId.classList.add('is-home');
                },
            },
        });
        mySwiper.on('slideChange', function () {
            if (this.realIndex === 0) {
                rootId.classList.add('is-home');
            } else {
                rootId.classList.remove('is-home');
            }
        });

        if (window.innerWidth > 991) {
            mySwiper.init();
        }

        ApiDefault.get(`land?per_page=3`)
            .then(res => {
                this.setState({ lands: res.data });
            })
            .catch(err => console.log(err));

        ApiDefault.get(`posts?per_page=3`)
            .then(res => {
                this.setState({ news: res.data });
            })
            .catch(err => console.log(err));
    }
    render() {
        var { lands, news } = this.state;
        return (
            <div className="p-home">
                <div className="c-swiper">
                    <div className="swiper-wrapper">
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/home/bg01.jpg)' }}></div>
                            <div className="p-home__logo" id="homelogo"><img src='/logo.svg' alt="Houzez" /></div>
                        </div>
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/home/bg02.jpg)' }}></div>
                            <div className="l-container">
                                <div className="p-home01">
                                    <div className="p-home01__about">
                                        <div className="c-block">
                                            <h3>Houzez</h3>
                                            <p>Sea tourist apartment <strong>5 Star Plus</strong>. Houzez offers the perfect weekend holiday, complete every moment in the true resort space</p>
                                            <Link to="/about">Read More</Link>
                                        </div>
                                    </div>
                                    <div className="p-home01__img">
                                        <span style={{ backgroundImage: 'url(images/home/about01.jpg)' }}></span>
                                        <span style={{ backgroundImage: 'url(images/home/about02.jpg)' }}></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/home/bg03.jpg)' }}></div>
                            <div className="l-container">
                                <div className="p-home02">
                                    <div className="c-block">
                                        <h3>Real Estate</h3>
                                        <p>The apartments are inspired by elegant, elegant and natural style, the apartments at <strong>Houzez</strong> are reasonably arranged, optimized for use, and bring a comfortable and trendy lifestyle. for the homeowner.</p>
                                        <Link to="/real-estate">Read More</Link>
                                    </div>
                                    <div className="p-home02__list">
                                        <Land lands={lands} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/home/bg04.jpg)' }}></div>
                            <div className="l-container">
                                <div className="p-home03">
                                    <div className="p-home03__cont">
                                        <ul>
                                            <li>
                                                <svg xmlns='http://www.w3.org/2000/svg' width='600' height='600' x='0' y='0' className='svgMask' viewBox='0 0 600 600'>
                                                    <defs>
                                                        <clipPath id='maskID0'>
                                                            <path d="M557.2,207.1c-24.2-21.7-54.5-35.1-84.3-47.8c-29.8-12.7-60.2-25.6-85-46.6c-22.6-19-39.5-43.9-60.8-64.4c-21.3-20.5-49.6-37.1-78.2-33c-28.7,4.1-50.2,27.9-65.1,52.7c-14.9,24.8-26.1,52.5-46.3,73.1c-26.1,26.7-64.5,38.6-90.9,65.1C18,235,6.9,278.7,11.8,319.7c4.9,41,24.5,79.5,50,112.4c25.5,32.9,56.7,60.7,88.2,87.8c18.3,15.7,36.9,31.4,58,42.9c21.1,11.4,45.3,18.5,68.8,15.6c37.1-4.5,66.6-32.4,89-62.2c22.4-29.8,40.9-63.2,69.5-87.1c27.9-23.3,63.3-35.5,94.9-53.7c31.6-18.1,61.4-46,65.4-82.6C599,260.8,581.4,228.9,557.2,207.1z"></path>
                                                        </clipPath>
                                                    </defs>
                                                    <image width='600' height='600' clipPath='url(#maskID0)' xlinkHref="images/home/uti01.jpg"></image>
                                                </svg>
                                            </li>
                                            <li>
                                                <svg xmlns='http://www.w3.org/2000/svg' width='600' height='500' x='0' y='0' className='svgMask' viewBox='0 0 600 500'>
                                                    <defs>
                                                        <clipPath id='maskID1'>
                                                            <path d="M431.1,22.5c25.1,1.9,50.6,3.9,73.9,13.3c43.6,17.4,74.4,59.4,86.2,104.8c11.8,45.4,6.4,93.9-6.7,139c-11.7,40.3-30,79.5-58.8,110.1c-28.8,30.6-68.9,52-110.9,52.2c-28.5,0.2-56.8-9.1-85.1-6.2c-45.8,4.7-82.5,39.6-126.3,53.7	c-45.6,14.6-98.4,4.4-135.2-26.3C27.9,429.7,8.8,376,7.4,323.7C6.1,271.5,20.6,220.2,38,170.9C58.8,112.2,88.2,50.3,144.4,23.6	c40.7-19.3,88-16.3,132.9-12.8C328.6,14.7,379.8,18.6,431.1,22.5z"></path>
                                                        </clipPath>
                                                    </defs>
                                                    <image width='600' height='500' clipPath='url(#maskID1)' xlinkHref="images/home/uti02.jpg"></image>
                                                </svg>
                                            </li>
                                            <li>
                                                <svg xmlns='http://www.w3.org/2000/svg' width='600' height='600' x='0' y='0' className='svgMask' viewBox='0 0 600 600'>
                                                    <defs>
                                                        <clipPath id='maskID2'>
                                                            <path d="M44.1,105.6c9.7-27.2,34.2-72.9,100.4-86.5c257.9-52.9,648,33.2,335.6,455.5C275.7,751-106,528.7,44.1,105.6z"></path>
                                                        </clipPath>
                                                    </defs>
                                                    <image width='600' height='600' clipPath='url(#maskID2)' xlinkHref="images/home/uti03.jpg"></image>
                                                </svg>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="c-block">
                                        <h3>Utilities</h3>
                                        <p>A system of relaxing utilities, high-class entertainment appeared for the first time, bringing a special experience to residents</p>
                                        <Link to="/utility">Read More</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/home/bg05.jpg)' }}></div>
                            <div className="l-container">
                                <div className="p-home04">
                                    <div className="c-block">
                                        <h3>News</h3>
                                    </div>
                                    <New news={news} />
                                </div>
                            </div>
                        </div>
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ 'background': 'linear-gradient(#285a71, #eedfc6)' }}></div>
                            <div className="l-container">
                                <div className="p-home05">
                                    <h3>Tập đoàn bất động sản <span>Virus Center</span></h3>
                                    <p>312/6 Đường HT13, Khu Phố 7, Phường Hiệp Thành, Quận 12</p>
                                    <p>Điện Thoại: <strong>0378 377 995</strong></p>
                                    <p>Email: <strong>v2s.viet.sy@gmail.com</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="c-swiper__arrow c-swiper__prev"><span></span><span></span><span></span></div>
                    <div className="c-swiper__arrow c-swiper__next"><span></span><span></span><span></span></div>
                    <div className="c-swiper__pagination"></div>
                </div>
            </div>
        );
    }
}

export default Home;