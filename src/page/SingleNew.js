import React, { Component } from 'react';
import ApiDefault from '../api/ApiDefault';

class SingleNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoader: false,
            title: '',
            date: '',
            content: '',
        };
    }

    getDate = (date) => {
        return date.split(' ')[0]
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        ApiDefault.get(`posts/${id}`)
            .then(res => this.setState({
                isLoader: true,
                title: res.data.title.rendered,
                date: res.data.news_date,
                content: res.data.content.rendered,
            }))
            .catch(err => console.log(err));
    }

    render() {
        var { isLoader, title, date, content } = this.state;
        return (
            <div className="p-single">
                <div className="p-single__bg" style={{ backgroundImage: 'url(../images/news/bg.jpg)' }}></div>
                {isLoader ?
                    <div className="p-new">
                        <h2 className="p-new__title">{title}</h2>
                        <p className="p-new__date">{date}</p>
                        <div className="p-new__content" dangerouslySetInnerHTML={{ __html: content }}></div>
                    </div> : <div className="c-loading"></div>
                }
            </div>
        );
    }
}

export default SingleNew;