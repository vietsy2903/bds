import React, { Component } from 'react';
import FormContact from '../components/Form/FormContact';
import FormThank from '../components/Form/FormThank';
import ApiCustom from '../api/ApiCustom';

class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isThank: false
        }
    }

    componentDidMount() {
        const rootId = document.getElementById("root");
        rootId.classList.remove('is-home');
        var htitle = document.getElementById("htitle");
        htitle.textContent = "Contact";
        htitle.style.animation = 'ani01-in 0.8s forwards ease-in-out';
    }
    onSubmit = (data) => {
        ApiCustom.post(`contact/v2/sent/`, data)
            .then(res => {
                if (res.statusText === 'OK') {
                    this.setState({
                        isThank: true,
                    });
                }
            })
    }
    render() {
        var { isThank } = this.state;
        return (
            <div className="p-page">
                <div className="p-page__bg" style={{ backgroundImage: 'url(images/contact/bg.jpg)' }}></div>
                <div className="p-contact">
                    {isThank ? <FormThank /> : <FormContact onSubmit={this.onSubmit} />}
                </div>
            </div>
        );
    }
}

export default Contact;