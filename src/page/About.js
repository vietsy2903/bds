import React, { Component } from 'react';
import Swiper from 'swiper';
import SwiperCore, { Pagination, Navigation, Mousewheel } from 'swiper/core';

SwiperCore.use([Pagination, Navigation, Mousewheel]);
class About extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
        const rootId = document.getElementById("root");
        rootId.classList.remove('is-home');
        var htitle = document.getElementById("htitle");
        htitle.textContent = "About";
        htitle.style.animation = 'ani01-in 0.8s forwards ease-in-out';
        var mySwiper = new Swiper(".c-swiper", {
            init: false,
            direction: "vertical",
            slidesPerView: 1,
            spaceBetween: 0,
            mousewheel: true,
            speed: 1000,
            loop: true,
            pagination: {
                el: '.c-swiper__pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.c-swiper__next',
                prevEl: '.c-swiper__prev',
            }
        });
        if (window.innerWidth > 991) {
            mySwiper.init();
        }
    }
    render() {
        return (
            <div className="p-about">
                <div className="c-swiper">
                    <div className="swiper-wrapper">
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/about/bg.jpg)' }}></div>
                            <div className="p-about__about">
                                <div className="p-about__content">
                                    <h3>project introduction</h3>
                                    <p>Houzez is designed to be a true and authentic Second Home. The Wave was born for the spontaneous trips of Saigon people, for the owner to find peace - peace - purity when tired, a place for the owner to re-energize after the days. long, stressful work.</p>
                                    <p>Not only possessing a position "more than diamond", Houzez also owns a unique architecture inspired by the winding waves of the sea. This design helps the project to retain its softness and sophistication, avoiding the rough motif like most existing buildings.</p>
                                </div>
                            </div>
                        </div>
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/about/bg01.jpg)' }}></div>
                            <div className="p-about__about p-about__about--child">
                                <div className="p-about__content p-about__content--child">
                                    <h3>Project scale</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="c-swiper__arrow c-swiper__prev"><span></span><span></span><span></span></div>
                    <div className="c-swiper__arrow c-swiper__next"><span></span><span></span><span></span></div>
                    <div className="c-swiper__pagination"></div>
                </div>
            </div>
        );
    }
}

export default About;