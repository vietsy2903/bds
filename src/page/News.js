import React, { Component } from 'react';
import New from '../components/New';
import ApiDefault from '../api/ApiDefault';

class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoader: false,
            news: []
        };
    }
    componentDidMount() {
        const rootId = document.getElementById("root");
        rootId.classList.remove('is-home');
        var htitle = document.getElementById("htitle");
        htitle.textContent = "News";
        htitle.style.animation = 'ani01-in 0.8s forwards ease-in-out';

        ApiDefault.get(`posts`)
            .then(res => {
                this.setState({ isLoader: true, news: res.data });
            })
            .catch(err => console.log(err));
    }
    render() {
        var { isLoader, news } = this.state;
        return (
            <div className="p-page">
                <div className="p-page__bg" style={{ backgroundImage: 'url(../images/news/bg.jpg)' }}></div>
                <div className="p-news">
                    {isLoader ? <New news={news} /> : <div className="c-loading"></div>}
                </div>
            </div>
        );
    }
}

export default News;