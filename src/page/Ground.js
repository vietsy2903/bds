import React, { Component } from 'react';
import Swiper from 'swiper';
import SwiperCore, { Pagination, Navigation, Mousewheel } from 'swiper/core';

SwiperCore.use([Pagination, Navigation, Mousewheel]);
class Ground extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
        const rootId = document.getElementById("root");
        rootId.classList.remove('is-home');
        var htitle = document.getElementById("htitle");
        htitle.textContent = "Ground";
        htitle.style.animation = 'ani01-in 0.8s forwards ease-in-out';

        var mySwiper = new Swiper(".c-swiper", {
            init: false,
            direction: "vertical",
            slidesPerView: 1,
            spaceBetween: 0,
            mousewheel: true,
            speed: 1000,
            loop: true,
            pagination: {
                el: '.c-swiper__pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.c-swiper__next',
                prevEl: '.c-swiper__prev',
            }
        });
        if (window.innerWidth > 991) {
            mySwiper.init();
        }
    }
    render() {
        return (
            <div className="p-utility">
                <div className="c-swiper">
                    <div className="swiper-wrapper">
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/ground/bg01.jpg)' }}></div>
                            <div className="p-utility__box">
                                <div className="p-utility__img">
                                    <svg xmlns='http://www.w3.org/2000/svg' width='600' height='600' x='0' y='0' className='svgMask' viewBox='0 0 600 600'>
                                        <defs>
                                            <clipPath id='maskID0'>
                                                <path d="M557.2,207.1c-24.2-21.7-54.5-35.1-84.3-47.8c-29.8-12.7-60.2-25.6-85-46.6c-22.6-19-39.5-43.9-60.8-64.4c-21.3-20.5-49.6-37.1-78.2-33c-28.7,4.1-50.2,27.9-65.1,52.7c-14.9,24.8-26.1,52.5-46.3,73.1c-26.1,26.7-64.5,38.6-90.9,65.1C18,235,6.9,278.7,11.8,319.7c4.9,41,24.5,79.5,50,112.4c25.5,32.9,56.7,60.7,88.2,87.8c18.3,15.7,36.9,31.4,58,42.9c21.1,11.4,45.3,18.5,68.8,15.6c37.1-4.5,66.6-32.4,89-62.2c22.4-29.8,40.9-63.2,69.5-87.1c27.9-23.3,63.3-35.5,94.9-53.7c31.6-18.1,61.4-46,65.4-82.6C599,260.8,581.4,228.9,557.2,207.1z"></path>
                                            </clipPath>
                                        </defs>
                                        <image width='600' height='600' clipPath='url(#maskID0)' xlinkHref="images/ground/ground01.png"></image>
                                    </svg>
                                </div>
                                <div className="p-utility__content">
                                    <h3><span>Ground 01</span></h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                            </div>
                        </div>
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/ground/bg02.jpg)' }}></div>
                            <div className="p-utility__box">
                                <div className="p-utility__img">
                                    <svg xmlns='http://www.w3.org/2000/svg' width='600' height='500' x='0' y='0' className='svgMask' viewBox='0 0 600 500'>
                                        <defs>
                                            <clipPath id='maskID1'>
                                                <path d="M431.1,22.5c25.1,1.9,50.6,3.9,73.9,13.3c43.6,17.4,74.4,59.4,86.2,104.8c11.8,45.4,6.4,93.9-6.7,139c-11.7,40.3-30,79.5-58.8,110.1c-28.8,30.6-68.9,52-110.9,52.2c-28.5,0.2-56.8-9.1-85.1-6.2c-45.8,4.7-82.5,39.6-126.3,53.7	c-45.6,14.6-98.4,4.4-135.2-26.3C27.9,429.7,8.8,376,7.4,323.7C6.1,271.5,20.6,220.2,38,170.9C58.8,112.2,88.2,50.3,144.4,23.6	c40.7-19.3,88-16.3,132.9-12.8C328.6,14.7,379.8,18.6,431.1,22.5z"></path>
                                            </clipPath>
                                        </defs>
                                        <image width='600' height='500' clipPath='url(#maskID1)' xlinkHref="images/ground/ground02.png"></image>
                                    </svg>
                                </div>
                                <div className="p-utility__content">
                                    <h3><span>Ground 02</span></h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                            </div>
                        </div>
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/ground/bg03.jpg)' }}></div>
                            <div className="p-utility__box">
                                <div className="p-utility__img">
                                    <svg xmlns='http://www.w3.org/2000/svg' width='600' height='600' x='0' y='0' className='svgMask' viewBox='0 0 600 600'>
                                        <defs>
                                            <clipPath id='maskID2'>
                                                <path d="M44.1,105.6c9.7-27.2,34.2-72.9,100.4-86.5c257.9-52.9,648,33.2,335.6,455.5C275.7,751-106,528.7,44.1,105.6z"></path>
                                            </clipPath>
                                        </defs>
                                        <image width='600' height='600' clipPath='url(#maskID2)' xlinkHref="images/ground/ground03.png"></image>
                                    </svg>
                                </div>
                                <div className="p-utility__content">
                                    <h3><span>Ground 03</span></h3>
                                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.<br />
                                    The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                                </div>
                            </div>
                        </div>
                        <div className="swiper-slide c-swiper__item">
                            <div className="c-swiper__bg" style={{ backgroundImage: 'url(images/ground/bg04.jpg)' }}></div>
                            <div className="p-utility__box">
                                <div className="p-utility__img">
                                    <svg xmlns='http://www.w3.org/2000/svg' width='600' height='500' x='0' y='0' className='svgMask' viewBox='0 0 600 500'>
                                        <defs>
                                            <clipPath id='maskID3'>
                                                <path d="M431.1,22.5c25.1,1.9,50.6,3.9,73.9,13.3c43.6,17.4,74.4,59.4,86.2,104.8c11.8,45.4,6.4,93.9-6.7,139c-11.7,40.3-30,79.5-58.8,110.1c-28.8,30.6-68.9,52-110.9,52.2c-28.5,0.2-56.8-9.1-85.1-6.2c-45.8,4.7-82.5,39.6-126.3,53.7	c-45.6,14.6-98.4,4.4-135.2-26.3C27.9,429.7,8.8,376,7.4,323.7C6.1,271.5,20.6,220.2,38,170.9C58.8,112.2,88.2,50.3,144.4,23.6	c40.7-19.3,88-16.3,132.9-12.8C328.6,14.7,379.8,18.6,431.1,22.5z"></path>
                                            </clipPath>
                                        </defs>
                                        <image width='600' height='500' clipPath='url(#maskID3)' xlinkHref="images/ground/ground04.png"></image>
                                    </svg>
                                </div>
                                <div className="p-utility__content">
                                    <h3><span>Ground 02</span></h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="c-swiper__arrow c-swiper__prev"><span></span><span></span><span></span></div>
                    <div className="c-swiper__arrow c-swiper__next"><span></span><span></span><span></span></div>
                    <div className="c-swiper__pagination"></div>
                </div>
            </div>
        );
    }
}

export default Ground;