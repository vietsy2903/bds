import React, { Component } from 'react';
import { GoogleMap, LoadScript, MarkerClusterer, Marker, InfoWindow } from '@react-google-maps/api';
import Land from '../components/Land';
import iconMap from '../assets/icon/mapMarker.svg';
import ApiDefault from '../api/ApiDefault';

class RealEstate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoader: false,
            lands: [],
            isOpen: false,
            showInfo: ''
        };
    }

    showInfoOpen = (markerId) => {
        this.setState({
            showInfo: markerId
        });
    }
    showInfoClose = (markerId) => {
        this.setState({
            showInfo: ''
        });
    };

    componentDidMount() {
        const rootId = document.getElementById("root");
        rootId.classList.remove('is-home');
        var htitle = document.getElementById("htitle");
        htitle.textContent = "Real Estate";
        htitle.style.animation = 'ani01-in 0.8s forwards ease-in-out';

        this.showInfoOpen();

        ApiDefault.get(`land`)
            .then(res => {
                this.setState({ isLoader: true, lands: res.data });
            })
            .catch(err => console.log(err));
    }
    render() {
        var { isLoader, lands } = this.state;
        var locations = [];
        for (let i = 0; i < lands.length; i++) {
            const item = lands[i];
            locations.push({
                lat: item.acf.map.lat,
                lng: item.acf.map.lng,
                price: item.acf.details.price
            });
        }
        var numberCenter = lands.length / 2;
        var center = locations[numberCenter.toFixed(0)];
        var styleMap = [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#444444"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#46bcec"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ];
        const options = { closeBoxURL: '', enableEventPropagation: true };
        return (
            <div className="p-page">
                <div className="p-page__bg" style={{ backgroundImage: 'url(images/home/bg03.jpg)' }}></div>
                {isLoader ?
                    <div className="p-realestate">
                        <div className="c-map">
                            <LoadScript googleMapsApiKey="AIzaSyBVE7BWWYfdZZ17452BwkaUPeBowp1qAUg" >
                                <GoogleMap
                                    zoom={15}
                                    center={center}
                                    mapContainerStyle={{ width: '100%', height: '100%' }}
                                    options={{
                                        styles: styleMap,
                                        scrollwheel: false,
                                        mapTypeControl: false,
                                        streetViewControl: false,
                                        fullscreenControl: true
                                    }}
                                >
                                    <MarkerClusterer>
                                        {(clusterer) =>
                                            locations.map((location, index) => (
                                                <Marker key={index} position={location} clusterer={clusterer} animation={2} icon={{ url: iconMap, scaledSize: { width: 50, height: 50 } }} onClick={() => this.showInfoOpen(index)}>
                                                    {(this.state.showInfo === index) && (
                                                        <InfoWindow options={options} onCloseClick={() => this.showInfoClose(index)}>
                                                            <div className="c-map__info"><span>$</span>{location.price}</div>
                                                        </InfoWindow>
                                                    )}
                                                </Marker>
                                            ))
                                        }
                                    </MarkerClusterer>
                                </GoogleMap>
                            </LoadScript>
                        </div>
                        <Land lands={lands} />
                    </div> : <div className="c-loading"></div>
                }
            </div>
        );
    }
}

export default RealEstate;