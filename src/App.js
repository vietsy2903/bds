import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './assets/scss/style.scss';
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './page/Home';
import About from './page/About';
import RealEstate from './page/RealEstate';
import SingleLand from './page/SingleLand';
import Utility from './page/Utility';
import Ground from './page/Ground';
import News from './page/News';
import SingleNew from './page/SingleNew';
import Contact from './page/Contact';

import Menu from './components/Menu';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDisplayMenu: false,
        }
    }

    showMenu = () => {
        if (this.state.isDisplayMenu === null) {
            this.setState({
                isDisplayMenu: true
            });
        } else {
            this.setState({
                isDisplayMenu: !this.state.isDisplayMenu
            });
        }
    }

    closeMenu = () => {
        this.setState({
            isDisplayMenu: !this.state.isDisplayMenu
        });
    }

    render() {
        var { isDisplayMenu } = this.state;
        // Menu
        var activeMenu = isDisplayMenu ? ' is-active' : '';
        var txtMenu = isDisplayMenu ? 'Close' : 'Menu';
        return (
            <Router>
                <div className="App">
                    {/* Header */}
                    <Header showMenu={this.showMenu} activeMenu={activeMenu} txtMenu={txtMenu} />

                    {/* Menu */}
                    <Menu activeMenu={activeMenu} closeMenu={this.closeMenu} />

                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/about" component={About} />
                        <Route exact path="/real-estate" component={RealEstate} />
                        <Route path="/real-estate/:id" component={SingleLand} />
                        <Route exact path="/utility" component={Utility} />
                        <Route exact path="/ground" component={Ground} />
                        <Route exact path="/news" component={News} />
                        <Route path="/news/:id" component={SingleNew} />
                        <Route exact path="/contact" component={Contact} />
                    </Switch>

                    {/* Footer */}
                    <Footer />
                </div>
            </Router>
        );
    }
}

export default App;
