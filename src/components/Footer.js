import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="c-footer">
                <p>2021 <strong>Houzez</strong>. All Rights Reserved.</p>
            </footer>
        );
    }
}

export default Footer;