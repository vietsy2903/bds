import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Land extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        var { lands } = this.props;
        var elmLands = lands.map((land, index) => {
            return (
                <div className="c-land__item" key={index}>
                    <Link to={`/real-estate/${land.id}`}>
                        <div className="c-land__img"><img src={land.image_src} alt={land.title.rendered} /></div>
                        <div className="c-land__cate">
                            {land.categories.map((category, index) => {
                                return (
                                    <span key={index}>{category.name}</span>
                                );
                            })}
                        </div>
                        <div className="c-land__body">
                            <h3 className="c-land__ttl">{land.title.rendered}</h3>
                            <div className="c-land__price"><span>from</span>${land.acf.details.price}</div>
                            <div className="c-land__text">{land.acf.description} <span>[more]</span></div>
                            <div className="c-land__info">
                                <ul>
                                    <li>
                                        <svg xmlns="http:www.w3.org/2000/svg" fill="none" viewBox="0 0 90 50">
                                            <path fill="#000" d="M16 18c0-4.4 3.6-8 8-8s8 3.6 8 8-3.6 8-8 8-8-3.6-8-8zm72 12H12V2c0-1.1-.9-2-2-2H2C.9 0 0 .9 0 2v48h12v-8h66v8h12V32c0-1.1-.9-2-2-2zM74 12H38c-1.1 0-2 .9-2 2v12h52c0-7.7-6.3-14-14-14z"></path>
                                        </svg>
                                        {land.acf.details.bedrooms}
                                    </li>
                                    <li>
                                        <svg xmlns="http:www.w3.org/2000/svg" fill="none" viewBox="0 0 56 59">
                                            <path fill="#000" d="M2 37a17.01 17.01 0 0010 15.474V58c.002.552.448.998 1 1h2a1.003 1.003 0 001-1v-4.281c.99.182 1.994.276 3 .281h18a16.98 16.98 0 003-.281V58c.002.552.448.998 1 1h2a1.003 1.003 0 001-1v-5.526A17.01 17.01 0 0054 37v-6H2v6zM55 27H1a1 1 0 100 2h54a1 1 0 100-2zM5 21h2v1a1 1 0 102 0v-4a1 1 0 10-2 0v1H5V7a5 5 0 0110 0v.09A6.002 6.002 0 0010 13c.002.552.448.998 1 1h10a1.003 1.003 0 001-1 6.002 6.002 0 00-5-5.91V7A7 7 0 103 7v18.5h2V21z"></path>
                                        </svg>
                                        {land.acf.details.bathrooms}
                                    </li>
                                    <li>
                                        <svg xmlns="http:www.w3.org/2000/svg" fill="none" viewBox="0 0 42 32">
                                            <path fill="#000" d="M41 0H13c-.55 0-1 .45-1 1v9H1c-.55 0-1 .45-1 1v20c0 .55.45 1 1 1h28c.55 0 1-.45 1-1v-9h11c.55 0 1-.45 1-1V1c0-.55-.45-1-1-1zM28 30H2V12h26v18zm12-10H30v-9c0-.55-.45-1-1-1H14V2h26v18z"></path>
                                        </svg>
                                        {land.acf.details.acreage} m<sup>2</sup>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </Link>
                </div>
            );
        });

        return (
            <div className="c-land">
                {elmLands}
            </div>
        );
    }
}

export default Land;