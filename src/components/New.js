import React, { Component } from 'react';
import { Link } from "react-router-dom";

class New extends Component {
    render() {
        var news = this.props.news;
        var elmNews = news.map((data, index) => {
            var fourItem = (index + 1) % 4 !== 0 ? '' : 'is-four';
            return (
                <li key={index} className={"c-new__item " + (fourItem)}>
                    <Link to={`/news/${data.id}`}>
                        <div className="c-new__block">
                            <div className="c-new__img"><img src={data.image_src} alt={data.title.rendered} /></div>
                            <div className="c-new__text">
                                <h3 className="c-new__ttl">{data.title.rendered}</h3>
                                <div className="c-new__btn">Read More</div>
                            </div>
                        </div>
                    </Link>
                </li >
            );
        });

        return (
            <ul className="c-new" >
                {elmNews}
            </ul>
        );
    }
}

export default New;