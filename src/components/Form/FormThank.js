import React, { Component } from 'react';
import { Link } from "react-router-dom";

class FormThank extends Component {
    render() {
        return (
            <div className="c-thank">
                <h3>Thank you!</h3>
                <p>Your email has been sent!<br />We will reply to you as soon as possible.</p>
                <Link to="/">Home</Link>
            </div>

        );
    }
}

export default FormThank;