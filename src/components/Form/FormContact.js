import React, { Component } from 'react';

class FormContact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSent: true,
            isConfirm: false,
            name: '',
            email: '',
            tel: '',
            message: ''
        }
    }
    onHandelChange = (event) => {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name]: value
        });
    }
    onConfirm = () => {
        if (this.state.name && this.state.email && this.state.tel && this.state.message) {
            this.setState({
                isConfirm: true,
            });
        }
    }
    onReset = () => {
        this.setState({
            isConfirm: false,
            name: '',
            email: '',
            tel: '',
            message: ''
        });
    }
    onBack = () => {
        this.setState({
            isConfirm: false,
        });
    }
    onSent = () => {

    }
    onSubmit = (event) => {
        event.preventDefault();
        this.props.onSubmit(this.state);
        this.setState({
            isSent: false,
        });
    }
    render() {
        var { isConfirm, isSent } = this.state;
        return (
            <div className="c-form">
                {isSent ? null : <div className="c-sending"></div>}
                <form onSubmit={this.onSubmit}>
                    {isConfirm ?
                        <div className="c-form__confirm">
                            <div className="c-form__row">
                                <span>Name:</span><p>{this.state.name}</p>
                            </div>
                            <div className="c-form__row">
                                <span>Email:</span><p>{this.state.email}</p>
                            </div>
                            <div className="c-form__row">
                                <span>Tel:</span><p>{this.state.tel}</p>
                            </div>
                            <div className="c-form__row">
                                <span>Message:</span><p>{this.state.message}</p>
                            </div>
                        </div> : <div className="c-form__inner">
                            <div className="c-form__row">
                                <input onChange={this.onHandelChange} value={this.state.name} name="name" type="text" placeholder="Name" />
                            </div>
                            <div className="c-form__row">
                                <input onChange={this.onHandelChange} value={this.state.email} name="email" type="email" placeholder="Email" />
                            </div>
                            <div className="c-form__row">
                                <input onChange={this.onHandelChange} value={this.state.tel} name="tel" type="tel" placeholder="Tel" />
                            </div>
                            <div className="c-form__row">
                                <textarea onChange={this.onHandelChange} value={this.state.message} name="message" placeholder="Message" />
                            </div>
                        </div>
                    }
                    <div className="c-form__btn">
                        {isConfirm ? <div className="c-form__back" onClick={this.onBack}>Back</div> : <div className="c-form__reset" onClick={this.onReset}>Reset</div>}
                        {isConfirm ? <button type="submit">Sent</button> : <div className="c-form__btnconfirm" onClick={this.onConfirm}>Confirm</div>}
                    </div>
                </form>

            </div >
        );
    }
}

export default FormContact;