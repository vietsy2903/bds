import React, { Component } from 'react';
import { Link } from "react-router-dom";
//import logo from '../logo.svg';

class Header extends Component {
    showMenu = () => {
        this.props.showMenu();
    }
    render() {
        var activeMenu = this.props.activeMenu;
        var txtMenu = this.props.txtMenu;
        return (
            <header className="c-header">
                <div className="c-header__call">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 40 40">
                            <path fill="currentColor" d="M23.8 28c1.3 0 2.8-.8 3.6-1.7.9-1 .4-1.7.3-1.8l-3-3c-.2-.2-.5-.4-.9-.4-.3 0-.6.1-.9.4l-.1.1c-.3.3-.6.6-.8 1.2-1.5-1.2-2.9-2.6-4.2-4.2.5-.2.9-.6 1.2-.8l.1-.1c.5-.5.5-1.3 0-1.7l-3.3-3.3c-.5-.5-1.2-.4-2 .2-1.2 1.1-2.3 3.7-1.3 5 3.1 3.9 6.4 7.3 9.9 9.9.4 0 .9.2 1.4.2zm-11-10.7z"></path>
                            <path fill="currentColor" d="M20 37.5c-9.6 0-17.5-7.9-17.5-17.5S10.4 2.5 20 2.5 37.5 10.4 37.5 20 29.6 37.5 20 37.5zm0-34C10.9 3.5 3.5 10.9 3.5 20S10.9 36.5 20 36.5 36.5 29.1 36.5 20 29.1 3.5 20 3.5z"></path>
                        </svg>
                    </span>
                    <a href="tel:0378377995">0378377995</a>
                </div>
                <div className="c-header__logo" id="logo">
                    <Link to="/"><img src='/logo.svg' alt="Houzez" /></Link>
                </div>
                <div className={"c-header__toggle" + (activeMenu)} onClick={this.showMenu}><span>{txtMenu}</span></div>
                <h2 className="c-header__title" id="htitle">Houzez</h2>
            </header>
        );
    }
}

export default Header;