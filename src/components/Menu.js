import React, { Component } from 'react';
import { Route, Link } from "react-router-dom";

const menus = [
    {
        name: 'Home',
        to: '/',
        exact: true
    }, {
        name: 'About',
        to: '/about',
        exact: false
    }, {
        name: 'Real Estate',
        to: '/real-estate',
        exact: false
    }, {
        name: 'Utility',
        to: '/utility',
        exact: false
    }, {
        name: 'Ground',
        to: '/ground',
        exact: false
    }, {
        name: 'News',
        to: '/news',
        exact: false
    }, {
        name: 'Contact',
        to: '/contact',
        exact: false
    }
];

//Custom Link
const MenuLink = ({ label, to, activeOnlyWhenExact }) => {
    return (
        <Route
            path={to}
            exact={activeOnlyWhenExact}
            children={({ match }) => {
                var active = match ? 'is-active' : '';

                return (
                    <li className={`${active}`}>
                        <Link to={to}>{label}</Link>
                    </li>
                );
            }}
        />
    );
}

class Menu extends Component {
    closeMenu = () => {
        this.props.closeMenu();
    }
    render() {
        var activeMenu = this.props.activeMenu;
        const width = activeMenu ? 'calc(100% - 320px)' : '0';
        const right = activeMenu ? '0' : '-320px';
        return (
            <nav className={"c-nav" + (activeMenu)}>
                <div className="c-nav__bg" style={{ 'width': width }}></div>
                <ul onClick={this.closeMenu} style={{ 'right': right }}>
                    {this.showMenus(menus)}
                </ul>
            </nav>
        );
    }

    showMenus = (menus) => {
        var result = null;

        if (menus.length > 0) {
            result = menus.map((menu, index) => {
                return (
                    <MenuLink
                        key={index}
                        label={menu.name}
                        to={menu.to}
                        activeOnlyWhenExact={menu.exact}
                    />
                );
            });
        }

        return result;
    }
}

export default Menu;