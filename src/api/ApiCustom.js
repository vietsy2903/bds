import axios from 'axios';

export default axios.create({
    baseURL: `https://home.api-fast.ml/wp-json/`,
});